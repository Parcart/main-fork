import { readable, writable } from 'svelte/store'

export const load_ini_data= writable(false) //индикатор загрузки ини-данных
export const ini_data = writable({}) // ини-данные
export const curr_year_id =writable(0) //текущий учебный год
export const curr_month = writable(0) //тек-й мес.
export const err_ini_data= writable(false) //сообщение об ошибке при загр. ини-данных
console.log("123")

export default  function (){
    const url_api= "https://api.ursei.su/public/schedule/rest/GetGSSchedIniData";
   
    async function get() {
        load_ini_data.set(true)
        const cd = new Date()
        
        
        try {
            const response = await fetch(url_api,{},3000)
            const js = await response.json();
            console.log("JS",js)

            if (js.hasOwnProperty('Error')){
               // обраб. ошибку остановки БД для архивации в 01.00
                throw js.Error
            }
            else{
              
                ini_data.set(await js.FormEdu)
              }
            
        } catch(e) {
            err_ini_data.set(e)
        }
        load_ini_data.set(false)
    }

    get()

}

export const group_data = writable({});
export const load_group_data = writable(false);
export function group_selected(group_id)

{
    const url_api= "https://api.ursei.su/public/schedule/rest/GetGsSched?grpid="+{group_id};
   
    async function get() {

        try {
            const response = await fetch(url_api,{},3000)
            const js = await response.json();
            console.log("JS",js)

            if (js.hasOwnProperty('Error')){

                throw js.Error
            }
            else{
              curr_month.set(currMonth)
              group_data.set(await js)
              }
            
        } catch(e) {
            err_ini_data.set(e)
        }
        load_group_data.set(false)
    }

    get()
}
