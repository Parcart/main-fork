// функции Hort и Vert можно переделать в одну

import { append } from "svelte/internal"

//Формируем строку элементов со знаком Х или 0 
export function checkHor(cnt,r, els,currMask,callback,setGame){
	console.log(setGame())
	console.log(cnt())
	for (let i = 0; i < cnt()-setGame()+1; i++){
	  let part = els[r].slice(i, i + setGame())
	  let partArr = part.map(x => x.innerText)
	  let isLine = partArr.toString() === currMask.toString() 
	  if(isLine){
		  part.forEach(e=>e.classList.add('line'))
		  callback();} 
	}
	
}
//Формируем столбец элементов со знаком Х или 0 
export function checkVert(cnt,c, els,currMask,callback,setGame){
		for (let i = 0; i < cnt()-setGame()+1; i++){
			let part = els.slice(i, i + setGame()).map(x=>x[c])
			let partArr = part.map(x => x.innerText)
			let isLine=partArr.toString() === currMask.toString()
			if(isLine){
				part.forEach(e=>e.classList.add('line'))
				callback();} 
		}
		
}
//Формируем девую диагональ элементов со знаком Х или 0 
export function checkLeftDiag(cnt,r,c, els,currMask,callback,setGame){
	let rows,startCol,startRow,pos
	if(r + c <= cnt()-1){
		startRow = 0
		startCol = r + c
		rows = els.slice(startRow,startCol +1)
	}else{
		startCol = cnt()-1
		startRow = r -(cnt()-1 - c)
		rows = els.slice(startRow,startCol+1)
	}
	pos = startCol 
	let diagArr=[]
	rows.forEach(x=>{diagArr.push(x[pos]); pos--;})
	setLine(diagArr,currMask,callback,setGame)
}
//Формируем правую диагональ элементов со знаком Х или 0 
export function checkRightDiag(cnt,r,c, els,currMask,callback,setGame){
	let rows,startCol,startRow,pos
	if(c - r <= 0){
		startRow = Math.abs(c - r)
		startCol = 0
		rows = els.slice(startRow,cnt())
	}else{
		startCol = c - r
		startRow = 0
		rows = els.slice(startRow,cnt() - startCol)
	}
	pos=startCol
	let diagArr=[]
	rows.forEach(x=>{diagArr.push(x[pos]); pos++;})
	setLine(diagArr,currMask, callback,setGame)
	 
}

//ищем пять Х или 0 в массиве эл-в, если находим даем стиль line

function setLine(diagArr,currMask,callback,setGame){
		for (let i = 0; i < diagArr.length-setGame()+1; i++){
	    let part = diagArr.slice(i, i + setGame())
	    let partArr = part.map(x => x.innerText)
		  let isLine = partArr.toString() === currMask.toString()
		  console.log(partArr, currMask)
		  if(isLine){
				part.forEach(e=>e.classList.add('line'))
                callback(); }
		}
		
		  
}
